import logging
import serial
from typing import Optional

from digi.xbee.devices import XBeeDevice, RemoteXBeeDevice, XBee64BitAddress, XBeeException

from dev.callable_device import CallableDevice

# ライブラリ側でのロギング設定
logger = logging.getLogger(__name__)


class XbeeSenderDevice(CallableDevice):
    """
    $ pip install digi-xbee

    See xbee lib manual.
    https://readthedocs.org/projects/xbplib/downloads/pdf/latest/
    https://xbplib.readthedocs.io/en/latest/index.html
    """

    def __init__(self, port: str = 'COM1', baud_rate: int = 9600):
        self._port = port
        self._baud_rate = baud_rate
        # デバイスが存在しなくても'digi.xbee.devices.XBeeDevice'が生成される
        self._local_dev = XBeeDevice(self._port, self._baud_rate)

    def __call__(self, data: str = '', address_low: Optional[str] = None) -> Optional[bool]:
        if not self.is_open():
            self.open()
        if self.is_open():
            if address_low is None:
                return self.send_broadcast(data)
            else:
                return self.send_remotedevice(data, address_low)
        return False

    def send_broadcast(self, data: str = '') -> bool:
        try:
            self._local_dev.send_data_broadcast(data)
            return True
        except XBeeException as e:
            logging.error(self.__class__.__name__ + ' : xbee bloadcast error.' + e)
            return False

    def send_remotedevice(self, data: str = '', address_low: str = '00000000') -> bool:
        try:
            address_high = '0013A200'
            # Instantiate a remote XBee dev object.
            remote_device = RemoteXBeeDevice(
                self._local_dev,
                XBee64BitAddress.from_hex_string(address_high + address_low)
            )
            # Send data using the remote object.
            self._local_dev.send_data(remote_device, data)

            return True

        except XBeeException as e:
            logging.error(self.__class__.__name__ + ' : xbee send error.' + e)
            return False

    def open(self) -> bool:
        return self.open_local_dev()

    def close(self) -> None:
        self.close_local_dev()

    def is_open(self):
        return self._local_dev.is_open()

    def open_local_dev(self) -> bool:
        if self.is_open():
            return True
        try:
            self._local_dev.open()
            return True
        except serial.SerialException as e:
            logging.error(self.__class__.__name__ + ' : xbee open error.')
            self._local_dev.close()  # openに失敗したときはとりあえずcloseしといた方がよさそう
            return False

    def close_local_dev(self) -> None:
        if self.is_open():
            self._local_dev.close()
