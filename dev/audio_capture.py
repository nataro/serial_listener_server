import logging
import pyaudio
import wave
from typing import Sequence, Optional, Dict, List

from dev.callable_device import CallableDevice

# ライブラリ側でのロギング設定
logger = logging.getLogger(__name__)


class AudioCaptureDevice(CallableDevice):
    """
    raspiでpyaudio環境を作るのはpipでpython3-pyaudioが一番簡単だが、
    自分作った環境にpipまたはpoetry addで入れるとなるとなかなか大変だと思う。
    """

    def __init__(self,
                 device_index: int = 0,
                 channels: int = 1,  # モノラル
                 rate: int = 44100,
                 # chunk: int = 2 ** 9,  #raspiで動かしたときは2**9にしないとoverflowとなった
                 chunk: int = 2 ** 11,
                 ):
        self._device_index = device_index
        self._channels = channels
        self._rate = rate
        self._chunk = chunk
        self._format = pyaudio.paInt16
        self._audio = None
        self._stream = None  # pyaudio.Stream

    @property
    def device_index(self):
        return self._device_index

    @device_index.setter
    def device_index(self, index: int):
        self._device_index = index

    def __call__(self,
                 file_path: str = 'file.wav',
                 device_index: Optional[int] = None,
                 seconds: float = 1.5,
                 ):
        self._device_index = self._device_index if device_index is None else device_index
        self.record(file_path, seconds)

    def record(self, file_path: str, seconds: float):
        self.open_rec_dev()
        frames = self.read_frames(seconds)
        self.close_rec_dev()
        self.write_wave_faile(file_path, frames)

    def open(self) -> bool:
        """
        このデバイスは録音時にデバイスをopenし、録音終了時に自動でcloseするように運用するので
        外部から操作できないようにこの関数は常にFalseを返すようにしておく
        """
        return False

    def close(self) -> None:
        """
        このデバイスは録音時にデバイスをopenし、録音終了時に自動でcloseするように運用するので
        外部から操作できないようにこの関数は常にFalseを返すようにしておく
        """
        return None

    def open_rec_dev(self) -> pyaudio.Stream:
        try:
            self._audio = pyaudio.PyAudio()
            self._stream = self._audio.open(
                format=self._format,
                channels=self._channels,
                rate=self._rate,
                input=True,
                input_device_index=self._device_index,
                frames_per_buffer=self._chunk,
            )
        except OSError as e:
            logging.debug(self.__class__.__name__ + e)

    def close_rec_dev(self):
        try:
            if not self._stream.is_stopped():
                self._stream.stop_stream()
                self._stream.close()
        except OSError as e:
            logging.debug(self.__class__.__name__ + e)

        self._audio.terminate()

    def write_wave_faile(self, file_path: str, frames: List):
        if self._audio is None: return

        with wave.open(file_path, 'wb') as wf:
            wf.setnchannels(self._channels)
            wf.setsampwidth(self._audio.get_sample_size(self._format))
            wf.setframerate(self._rate)
            wf.writeframes(b''.join(frames))

    def read_frames(self, seconds: float = 1.0) -> Optional[List]:
        try:
            if self._stream.is_active():
                frames = []
                for i in range(0, int(self._rate / self._chunk * seconds)):
                    data = self._stream.read(self._chunk)
                    frames.append(data)
                return frames

        except OSError as e:
            logging.debug(self.__class__.__name__ + e)
            return None

    @staticmethod
    def get_devices_dict() -> Dict:
        """
        接続されているオーディオ機器の情報を辞書型で取得する
        :return:
        """
        p = pyaudio.PyAudio()
        count = p.get_device_count()
        devices = {}
        for i in range(count):
            devices[i] = p.get_device_info_by_index(i)['name']
        return devices

    @staticmethod
    def get_index_list_by_name(device_name: str) -> List:
        """
        :param device_name:検索するデバイスの名称をstr型で指定
        :return: 該当するデバイスのindexをLIST型で返す(同じ名前のものが複数存在することもある)
        """
        device_dict = AudioCaptureDevice.get_devices_dict()
        keys = [k for k, v in device_dict.items() if v == device_name]
        return keys


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """
    import time

    ad = AudioCaptureDevice()
    print(ad.get_devices_dict())

    dev_name = 'ライン (Roland Rubix 24)'
    print(ad.get_index_list_by_name(dev_name))
    # [1, 7, 17]というリストが返ってきた（同じ名前のものが複数ある）。

    index = 1
    # ad.device_index = index  # 事前にindexを指定することも可能

    print('start')

    ad(file_path='tset_1.wav', device_index=1, seconds=3.5)

    time.sleep(1)

    ad(file_path='tset_2.wav', seconds=5.5)

    print('finish')
