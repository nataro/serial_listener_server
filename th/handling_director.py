import logging
from datetime import datetime
from pathlib import Path
import os

from abc import ABCMeta, abstractmethod

from th.device_handling import DeviceHandlingBuilderReceive, PicoHandlingBuilder, MicroCapHandlingBuilder

# ライブラリ側でのロギング設定
logger = logging.getLogger(__name__)


class RecordHandlingDirector(metaclass=ABCMeta):
    """
    call()で受け取ったdictを,self._builder(DeviceHandlingBuilderReceive)の
    check_dict関数で解析し、保存用のファイル名を生成する。

    生成したファイル名は、self.handling()関数に渡され、その関数の中で、
    self._builder.handling_thread()を用いてデータ保存スレッドを起動する
    """

    def __init__(
            self,
            builder: DeviceHandlingBuilderReceive,
    ):
        self._builder = builder

    def __call__(self, *args, **kwargs):
        """
        __call__()でメインの動作ができるようにしておく
        """
        (received_dict,) = args
        # if not any(received_dict):
        if any(received_dict):  # dictが空かどうかのチェック
            logger.debug(self.__class__.__name__ + ': __call__() received : ', received_dict)
            file_name = self._builder.check_dict(received_dict)
            if file_name is not None:
                self.handling(file_name, received_dict)

    @abstractmethod
    def handling(self, file_name: str = '', received: dict = None) -> None:
        """
        self._builder.handling_thread()を呼び出し、データ保存スレッドを生成する処理を記述すること。
        """
        pass

    def device_open(self) -> bool:
        return self._builder.device_open()

    def device_close(self) -> None:
        self._builder.device_close()

    def device_builder_is_ok(self) -> bool:
        return self._builder.state_is_ok


class PicoHandlingDirector(RecordHandlingDirector):
    """
    TODO:
        self.handlig()が受け取ったファイル名(file_name)によって、保存ディレクトリを変えるような処理を追加したい
        例えば、file_nameの先頭が'buffer_'という文字であった場合、バッファ保存用のディレクトリにファイルを保存する
        その後、バッファ保存用のディレクトリに格納されているファイルの数が、所定のファイル数より多ければ、最も古いファ
        イルを削除するという手順を実施することで、リングバッファ的な処理を動的なメモリを消費せずに実現できる気がする
        -> 必要な処理
        class PicoHandlingBuilder(DeviceHandlingBuilderReceive):のcheck_dict()関数内で、条件分けと
        'buffer_'という文字を追加する処理
    """

    def __init__(
            self,
            builder: PicoHandlingBuilder,
            dir_path: str = None,
            buffer_dir_path: str = None,
            buffer_file_num: int = 20,
    ):
        super().__init__(builder)
        self._buffer_file_head = builder.buffer_file_head
        self._buffer_file_num = buffer_file_num
        self._buffer_path = self.create_buff_directory(buffer_dir_path)
        self._storage_path = self.create_storage_directory(dir_path)

    def handling(self, file_name: str = '', received: dict = None) -> None:
        # バッファ用のファイル名が指定された状態で、ファイル名がバッファ用のファイル名に該当する場合
        if self._buffer_file_head is not None and file_name.startswith(self._buffer_file_head):
            # バッファ用のディレクトリに格納
            self._builder.handling_thread(dir_path=self._buffer_path, file_name=file_name, received=received)
            # バッファ用のディレクトリ内を確認し、指定個数を超えている場合は古いファイルを削除する
            self.delete_old_file(self._buffer_path, self._buffer_file_num)
        # バッファ用のファイル名が指定されていない場合や、ファイル名がバッファ用ファイルでない場合
        else:
            # 、通常の指定された保存先にデータを格納
            self._builder.handling_thread(dir_path=self._storage_path, file_name=file_name, received=received)

        # # バッファ用のファイル名が指定されていない場合は、通常の指定された保存先にデータを格納
        # if self._buffer_file_head is None:
        #     self._builder.handling_thread(dir_path=self._storage_path, file_name=file_name, received=received)
        # elif file_name.startswith(self._buffer_file_head):
        #     # バッファ用のディレクトリに格納
        #     self._builder.handling_thread(dir_path=self._buffer_path, file_name=file_name, received=received)
        #     # バッファ用のディレクトリ内を確認し、指定個数を超えている場合は古いファイルを削除する
        #     self.delete_old_file(self._buffer_path, self._buffer_file_num)

    @staticmethod
    def create_buff_directory(dir_path: str = None) -> str:
        """
        'buffer' という階層のディレクトリを生成する
        Parameters
        ----------
        dir_path : str
            配置するディレクトリ
            （指定しない場合はカレントディレクトリになる）

        Returns
        -------
        str
            生成したディレクトリのパス
            （配置するディレクトリは自動生成される）
        """
        p_dir_path = Path(dir_path) if dir_path else Path.cwd()
        p_dir_path = p_dir_path / 'buffer'

        # ディレクトリを生成
        p_dir_path.mkdir(exist_ok=True, parents=True)

        return str(p_dir_path.resolve())

    @staticmethod
    def create_storage_directory(dir_path: str = None) -> str:
        """
        インスタンスを生成した時点での/年/月/日という階層のディレクトリを生成する
        Parameters
        ----------
        dir_path : str
            配置するディレクトリ
            （指定しない場合はカレントディレクトリになる）

        Returns
        -------
        str
            生成したディレクトリのパス
            （配置するディレクトリは自動生成される）

        """
        p_dir_path = Path(dir_path) if dir_path else Path.cwd()

        dt = datetime.now()
        y = dt.strftime('%Y')
        m = dt.strftime('%m')
        d = dt.strftime('%d')

        p_dir_path = p_dir_path / y / m / d

        # ディレクトリを生成
        # exist_ok : 既に存在していてもエラーにならない
        # parents : 階層が深くても再帰的に生成
        p_dir_path.mkdir(exist_ok=True, parents=True)

        return str(p_dir_path.resolve())

    @staticmethod
    def delete_old_file(dir_path: str, max_num: int, suffix: str = '.*'):
        wild_card = '*' + suffix
        p_dir_path = Path(dir_path).glob(wild_card)

        # ファイルの更新日付でソート（index=0側が新しい）
        paths = sorted(p_dir_path, key=os.path.getmtime, reverse=True)

        # 指定されているファイル数よりも多い場合はファイルを削除する
        for i, p in enumerate(paths):
            if i > max_num - 1:
                os.remove(p)


class MicroCapHandlingDirector(RecordHandlingDirector):
    """
    """

    def __init__(
            self,
            builder: MicroCapHandlingBuilder,
            dir_path: str = None,
    ):
        super().__init__(builder)
        self._storage_path = self.create_storage_directory(dir_path)

    def handling(self, file_name: str = '', received: dict = None) -> None:
        self._builder.handling_thread(dir_path=self._storage_path, file_name=file_name, received=received)

    @staticmethod
    def create_storage_directory(dir_path: str = None) -> str:
        """
        インスタンスを生成した時点での/年/月/日という階層のディレクトリを生成する
        Parameters
        ----------
        dir_path : str
            配置するディレクトリ
            （指定しない場合はカレントディレクトリになる）

        Returns
        -------
        str
            生成したディレクトリのパス
            （配置するディレクトリは自動生成される）

        """
        p_dir_path = Path(dir_path) if dir_path else Path.cwd()

        dt = datetime.now()
        y = dt.strftime('%Y')
        m = dt.strftime('%m')
        d = dt.strftime('%d')

        p_dir_path = p_dir_path / y / m / d

        # ディレクトリを生成
        # exist_ok : 既に存在していてもエラーにならない
        # parents : 階層が深くても再帰的に生成
        p_dir_path.mkdir(exist_ok=True, parents=True)

        return str(p_dir_path.resolve())


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """

    import time
    from dev.cam_capture import CaptureDevice, MicroscopeCaptureDevice
    from dev.pico2000_capture import Ps2000Block
    from dev.pico5000_capture import Ps5000Block
    from th.device_handling import MicroCapHandlingBuilder, PicoHandlingBuilder

    # threshold = 10  # [mv]
    # samples = 2 ** 19
    # pico_dev = Ps2000Block(th_mv=threshold, num_samples=samples)
    # pico_hd = PicoHandlingBuilder(pico_dev, buffer_file_head='buff_')
    # pico_d = PicoHandlingDirector(pico_hd)

    threshold = 10000  # [mv]
    auto_trg_ms = 200
    range_mode = 'PS5000A_10V'
    samples = 2 ** 19

    pico_dev = Ps5000Block(
        th_mv=threshold,
        num_samples=samples,
        auto_trigger_time_ms=auto_trg_ms,
        range_mode=range_mode,
    )

    # pico_dev = Ps5000Block(
    #     th_mv=1000,
    #     num_samples=samples,
    #     auto_trigger_time_ms=5000,
    #     trigger_is_enable=False,
    #     # call_delay_ms=None,
    #     call_delay_ms=10000,
    # )

    pico_hd = PicoHandlingBuilder(pico_dev, buffer_file_head='buff_')
    pico_d = PicoHandlingDirector(pico_hd)

    # cap_dev = CaptureDevice(cam_num=0)
    cap_dev = MicroscopeCaptureDevice(cam_num=0)
    cap_hd = MicroCapHandlingBuilder(cap_dev)
    rec_d = MicroCapHandlingDirector(cap_hd)

    # ダミーの受信データ
    rec_dict = {'AC': 10.0, 'ST': 4.0, 'ZP': -0.08, }
    # rec_dict = {'CC': 10.0, }

    print('open')
    # RecordHandlingDirectorが保持してるデバイスを開いて計測準備を行う
    pico_d.device_open()
    rec_d.device_open()

    time.sleep(5)

    print('call')

    # 操作の実行
    pico_d(rec_dict)
    rec_d(rec_dict)

    time.sleep(15)  # 計測が実行される前にcloseするとエラーになるので待機

    # RecordHandlingDirectorが保持してるデバイスを閉じる
    pico_d.device_close()
    rec_d.device_close()

    print('closed')
