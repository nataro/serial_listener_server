import logging
import threading
from datetime import datetime
from abc import ABCMeta, abstractmethod
from typing import Sequence, Optional, Dict, List
import re

import numpy
import cv2

from dev.callable_device import CallableDevice
from util.pico_data import PicoDataH5FileSave, PicoData
from util.line_notify import send_message

# ライブラリ側でのロギング設定
logger = logging.getLogger(__name__)


class DeviceHandlingBuilder(metaclass=ABCMeta):
    """
    ここを参考に
    https://qiita.com/__init__/items/74b36eba31ccbc0364ed
    """

    def __init__(self, device: CallableDevice):
        self._device = device
        self._event = threading.Event()  # 計測処理に関するスレッドを同期して開始するために使用
        self._lock = threading.Lock()  # 複数のスレッドが同時に録音デバイスにアクセスしないようにロックする
        self._state_is_ok = True  # デバイス自体の状態や、取得したデータが正常かどうかを格納する

    @property
    def state_is_ok(self):
        return self._state_is_ok

    @state_is_ok.setter
    def state_is_ok(self, state: bool):
        self._state_is_ok = state

    def device_open(self) -> bool:
        return self._device.open()

    def device_close(self) -> None:
        self._device.close()

    def handling_thread(self, **kwargs):
        handling_t = threading.Thread(
            name=self.__class__.__name__ + '-handling_thread',
            target=self.handle_via_device,
            args=(self._event, self._lock,),
            kwargs=kwargs
        )
        handling_t.start()

        # 2021/02/19 タイミングがずれてうまく計測できないことがあるみたいなので、event.set()とwait()を一旦中止する
        # 複数種類のスレッドを同時に実行させたいことがあるかもしれないので、event.set()でタイミングをあわせる予定
        self._event.set()  # event.wait()で待機しているプロセスが全部同時に解除される

    @abstractmethod
    def handle_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          **kwargs, ):
        pass


class DeviceHandlingBuilderReceive(DeviceHandlingBuilder):
    """
    thredを実行する際に辞書型などで情報を渡して、その情報をthred処理内で利用する場合、
    受け取った辞書型の内容についてバリデーションを行うための関数を追加したもの。
    """

    @abstractmethod
    def check_dict(self, received: Dict) -> str:
        pass

    @staticmethod
    def keys_check(target_keys: Sequence, must_keys: Sequence) -> bool:
        """
        target_keysの中に、must_keysが全部含まれているか確認する
        """
        return set(target_keys) >= set(must_keys)


class MicroCapHandlingBuilder(DeviceHandlingBuilderReceive):
    def handle_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          received: dict = None,
                          dir_path: str = '',
                          file_name: str = '',
                          ext: str = 'jpg'):
        logging.debug(self.__class__.__name__ + ' : thread start')

        # 2021/02/19 タイミングがずれてうまく計測できないことがあるみたいなので、event.set()とwait()を一旦中止する
        event.wait()  # event.set()が実行されるまで待機

        logging.debug(self.__class__.__name__ + ' : do event')
        with lock:  # deviceへのアクセスを制限
            logging.debug(self.__class__.__name__ + ' : lock')
            dt = datetime.now()
            file_path = dir_path + "/{0}.".format(dt.strftime("%Y%m%d-%H%M%S-%f_") + file_name) + ext

            data = self._device()
            # if data is not None:
            #     # ボケ画像の判定処理を追加
            #     self._state_is_ok = False if self.is_blur_img(data) else True
            #     cv2.imwrite(file_path, data)

        logging.debug(self.__class__.__name__ + ' : unlock')

        if data is not None:
            # 20210826 画像による折損判定は一時中断
            # # ボケ画像の判定処理を追加
            # self._state_is_ok = False if self.is_blur_img(data) else True

            cv2.imwrite(file_path, data)
            # line 通知
            if not self._state_is_ok:
                send_message('errer on :' + file_path)

    def check_dict(self, received: Dict) -> Optional[str]:
        """
        received = {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}
        というような辞書型を受け取って、データを保存するかどうか判断する
        データを保存する場合はself.recording()を呼び出す

        Parameters
        ----------
        received : dict
            e.g. {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}

        """
        must_keys = ('CC',)
        is_involve = self.keys_check(list(received.keys()), must_keys)
        if not is_involve:
            return None

        counts = received.get('CC')  # 穴加工数を'CC'というkeyで受け取る予定
        try:
            counts = int(counts)
        except (ValueError, TypeError) as e:
            logging.error(e)
            # print('loggin check value error')
        else:
            file_name = "cc{0}".format(counts)
            return file_name
        return None

    # @staticmethod
    # def is_blur_img(img: numpy.ndarray, thresh: float = 1650000) -> bool:
    #     # def is_blur_img(img: numpy.ndarray, thresh: float = 165000) -> bool:
    #     # def is_blur_img(img: numpy.ndarray, thresh: float = 4500) -> bool:  # cv2.CAP_DSHOW
    #     """
    #     :param img : numpy.ndarray
    #         opencvで読み込んだ画像データ
    #     :param thresh : int
    #         ラプラシアンフィルタを掛けて、その値の分散値がこの閾値より低い場合、ボケ画像と判断する
    #
    #     リング照明なしのとき：カーネル5で閾値4500
    #     リング照明有りのとき：カーネル7で閾値200000
    #     """
    #     # ksize = 3
    #     ksize = 5
    #     # ksize = 7
    #     img = cv2.GaussianBlur(img, (ksize, ksize), 0)
    #     blur_value = cv2.Laplacian(img, cv2.CV_64F, ksize=ksize).var()
    #     is_lower = blur_value < thresh
    #     if is_lower:
    #         logging.error("blur value is {0}. thresh is {1}".format(blur_value, thresh))
    #
    #     return is_lower

    @staticmethod
    def is_blur_img(img: numpy.ndarray, thresh: float = 10) -> bool:
        """
        :param img : numpy.ndarray
            opencvで読み込んだ画像データ
        :param thresh : int
            ラプラシアンフィルタ（ボケの強弱）は使わずに、画像の分散値のみで折損を判定

        リング照明ありの状態
        ピントが逃げ面の下端に合った状態：19.10
        そこから1mm上方に工具を逃がした状態：16.17
        """
        mean, stddev = cv2.meanStdDev(img)
        is_lower = stddev.mean() < thresh
        if is_lower:
            logging.error("blur value is {0}. thresh is {1}".format(stddev.mean(), thresh))
        return is_lower


class PicoHandlingBuilder(DeviceHandlingBuilderReceive):
    """
    PicoScopeで計測データをHDF5形式で保存する

    """

    def __init__(self, device: CallableDevice, buffer_file_head: str = None, processing_check: bool = False):
        super().__init__(device)
        # self._must_record_count = list(range(0,50))  # 条件によらず必ず保存する穴数のlist
        self._must_record_count = list(range(0, 3))  # 条件によらず必ず保存する穴数のlist
        # self._record_count_interval = 1  # 何count毎に録音するか
        self._record_count_interval = 5  # 何count毎に録音するか
        # self._record_steps = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]  # 何step目を録音対象とするか
        # self._record_steps = [3, 4, 5]  # 何step目を録音対象とするか
        # self._record_steps = [0, 1, 2, 3, 4, 5]  # 何step目を録音対象とするか
        # self._record_steps = [0, 1, 3, 5, 7, 9]  # 何step目を録音対象とするか
        self._record_steps = [0, 1, 5]  # 何step目を録音対象とするか
        # self._record_steps = [2, 4, 6, 8, 10, 12, 14, ]  # 何step目を録音対象とするか
        self._file_save_tool = PicoDataH5FileSave()  # ファイル保存用の自作ツール(拡張子は.hdf5もしくは.h5にすること）
        self._buffer_file_head = buffer_file_head  # バッファ用のデータとして区別したい場合にファイル名の先頭につける
        self._processing_check = processing_check
        # self._not_processing_check_step = [0]  # 空加工チェックの対象外のステップを指定
        self._not_processing_check_step = None  # 空加工チェックの対象外のステップを指定

    @property
    def buffer_file_head(self):
        return self._buffer_file_head

    def handle_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          received: dict = None,
                          dir_path: str = '',
                          file_name: str = '',
                          ext: str = 'hdf5'):
        """
        Parameters
        ----------
        event : threading.Event
        lock : threading.Lock
        device : CallableMeasurement
        dir_path : str
        file_name : str
        received : dict
            マシニングセンタからシリアル通信で受け取った情報
        ext : str
        """
        logging.debug(self.__class__.__name__ + ' : thread start')

        # 2021/02/19 タイミングがずれてうまく計測できないことがあるみたいなので、event.set()とwait()を一旦中止する
        event.wait()  # event.set()が実行されるまで待機

        logging.debug(self.__class__.__name__ + ' : do event')
        with lock:  # deviceへのアクセスを制限
            logging.debug(self.__class__.__name__ + ' : lock')
            dt = datetime.now()
            file_path = dir_path + "/{0}.".format(dt.strftime("%Y%m%d-%H%M%S-%f_") + file_name) + ext

            # データの収集(PicoData型)
            data = self._device()
            # データ(PicoData型)にマシニングから受け取った情報を追加する
            data.machining_condition = received
            # 取得したデータがオーバーフローしていないかなどを確認
            self.pico_data_error_check(data, file_path)
            # データの保存
            self._file_save_tool(data, file_path)

            # print('pm:' + file_name)

        logging.debug(self.__class__.__name__ + ' : unlock')

        # 対象外のステップのファイル名の場合は（「*_st0.0*」という文字がファイル名に含まれている）の場合は空加工チェックを実施せずに終了
        if self.is_ac_step_file(file_name, self._not_processing_check_step):
            return

        # 空加工チェック
        if data is not None and self._processing_check:
            # 空加工判定処理を追加
            # PicoData.dataの中身をチェック
            self._state_is_ok = False if self.is_nonprocessing_data(data.data) else True
            # line 通知
            if not self._state_is_ok:
                send_message('nonprocessing on :' + file_path)

    def check_dict(self, received: Dict) -> Optional[str]:
        """
        received = {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}
        というような辞書型を受け取って、データを保存するかどうか判断する
        データを保存する場合はself.recording()を呼び出す

        Parameters
        ----------
        received : dict
            e.g. {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}

        """
        must_keys = ('AC', 'ST', 'ZP',)
        is_involve = self.keys_check(list(received.keys()), must_keys)
        if not is_involve:
            return None

        counts = received.get('AC')  # 穴加工数を'AC'というkeyで受け取る予定
        steps = received.get('ST')  # ステップ数を'ST'というkeyで受け取る予定
        depth = received.get('ZP')  # Z座標を'ZP'というkeyで受け取る予定
        try:
            counts = int(counts)
        except (ValueError, TypeError) as e:
            logging.error(e)
            # print('loggin check value error')
        else:
            file_name = "ac{0}_st{1}_zp{2:.3f}".format(counts, steps, depth)
            if (counts in self._must_record_count) or (counts % self._record_count_interval == 0):
                if steps in self._record_steps:
                    # file_name = "ac{0}_st{1}_zp{2:.3f}".format(counts, steps, depth)
                    return file_name
            if self._buffer_file_head is not None:
                # 条件に該当するもの以外は、ファイル名の先頭に特定の文字をつけて返す
                # (self._buffer_file_headが指定されている場合は全データが対象)
                return self._buffer_file_head + file_name
        return None

    @staticmethod
    def pico_data_error_check(picodata: PicoData, info: str) -> None:
        """
        Parameters
        ----------
        picodata : PicoData
        info : str
        """
        assigned_dict = picodata.assigned_values
        if not assigned_dict:
            return

        over_flow = assigned_dict.get('over_flow')
        trg_auto_ms = assigned_dict.get('trigger_auto_ms')
        run_time_ms = assigned_dict.get('run_block_time_ms')
        run_elp_time_sec = assigned_dict.get('run_block_elapsed_time_sec')

        if over_flow:
            logger.error('over flow at :' + info)

    @staticmethod
    def is_nonprocessing_data(data: numpy.ndarray, thresh: float = 70) -> bool:
        """
        :param data: picoscopeで読み取った数値
        :param thresh: mv
        :return:

        picoscopeで読み取ったデータが空加工のデータになっていないかをチェック

        標準偏差：np.std(data_pro)
        実効値：np.sqrt(np.square(data).mean())
        が考えられるが、今回、サンプルデータで確認したケースでは標準偏差の方が差がでそう
                         加工中     非加工
        標準偏差(SD)    73.2mv      26.0mv
        実効値(RMS)     79.1mv      73.2mv

        20210712に非加工と加工時の値を比較したら、50mvと300mvくらいだった？→閾値を60mvに変えた
        20210729 10db20V で　50mv
        20210730 20dv20v で　60mvに戻す
        20210824 センサ近傍(35mmくらい)の場合、空加工でも75~80になることがわかったので85mvに変えた
        20210901 ワークを広くして再近傍でも45mmくらい（60arr）なので70にした

        """
        np_val = numpy.std(data)
        is_lower = np_val < thresh
        if is_lower:
            logging.error("nonprocessing value is {0}. thresh is {1} mv.".format(np_val, thresh))
        return is_lower

    @staticmethod
    def is_ac_step_file(file_name: str, step_list: Optional[List[int]] = None) -> bool:
        """
        「*_st{i}.0*」という文字がファイル名に含まれているかを確認する
        {i}はリストにしたintで指定
        """
        if step_list is None:
            return False
        for i in step_list:
            pattern = f"_st{i}.0_"
            repatter = re.compile(pattern)
            result = repatter.search(file_name)
            if result is not None:
                return True
        return False
